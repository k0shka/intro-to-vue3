app.component('product-display', {
    props: {
        premium: {
            type: Boolean,
            required: true
        }
    },
    template:
    /*html*/
    `<div class="product-display">
    <div class="product-container">
      <div class="product-image">
        <img :class="{ 'out-of-stock-img': !inStock }" :src="image" alt="image">
      </div>
      <div class="product-info">
        <h1>{{ title }}</h1>
        <p>{{ sale }}</p>
        <p v-if="inStock">In Stock</p>
        <p v-else>Out of Stock</p>

        <p>Shipping: {{ shipping }}</p>

        <p v-if="onSale">On Sale!</p>

        <ul>
          <li v-for="detail in details">{{detail}}</li>
        </ul>

        <ul>
          <li v-for="(size, index) in sizes" :key="index">{{ size }}</li>
        </ul>

        <div v-for="(variant, index) in variants" 
            :key="variant.id"
            @mouseover="updateVariant(index)"
            class="color-circle"
            :class="{active: activeClass}"
            :style="{backgroundColor: variant.color}">
        </div>

        <button 
          class="button" 
          :class="{ disabledButton: !inStock }"
          :disabled="!inStock"
          @click="addToCart"
          >Add to Cart
        </button>
        <button 
        class="button" 
        :class="{ disabledButton: !inStock }" 
        :disabled="!inStock" 
        @click="removeFromCart">
        Remove Item
      </button>
      </div>
    </div>

    <review-list v-if="reviews.length" :reviews="reviews"></review-list>
    <review-form @review-submitted="addReview"></review-form>
    
  </div>`,
    data() {
        return {
            product: 'Socks',
            brand: 'Adibas',
            url: 'https://www.vuemastery.com/',
            inventory: 100,
            onSale: true,
            details: [
                '50% cotton',
                '30% wool',
                '20% polyester'
            ],
            sizes: ['S', 'M', 'L', 'XL'],
            variants: [
                { id: 2234, color: 'green', image: './assets/images/socks_green.jpg', quantity: 50},
                { id: 2235, color: 'MidnightBlue', image: './assets/images/socks_blue.jpg', quantity: 25}
            ],
            isActive: true,
            selectedVariant: 0,
            reviews: []
        }
    },
    methods: {
        updateImage(variantImage){
            this.image = variantImage
        },
        updateVariant(index){
            this.selectedVariant = index
        },
        addToCart(){
            this.$emit('add-to-cart', this.variants[this.selectedVariant].id)
        },
        removeFromCart() {
            this.$emit('remove-from-cart', this.variants[this.selectedVariant].id)
        },
        addReview(review) {
            this.reviews.push(review)
        }
    },
    computed: {
        title(){
            return this.brand + ' ' + this.product
        },
        image(){
            return this.variants[this.selectedVariant].image
        },
        inStock(){
            return this.variants[this.selectedVariant].quantity
        },
        sale(){
            if (this.onSale){
                return this.brand + ' ' + this.product + ' is on sale.'
            }

            return ''
        },
        shipping(){
            if (this.premium === true) {
                return 'Free'
            }
            return 'P 49.99'
        }
    }
})